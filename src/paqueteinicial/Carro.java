package paqueteinicial;

import interfaces.CarroInterface;

public class Carro implements CarroInterface {
    String marcaCarro;

    public Carro() {
        System.out.println("Construyendo carro");
    }

    public void asignarMarcaAlCarro(String marca) {
        marcaCarro = marca;
    }

    public void imprimirMarca() {
        System.out.println("La marca de mi carro es: " + marcaCarro);
    }

    @Override
    public void acelerar() {

    }

    @Override
    public void desacelerar() {

    }

    public String devuelveSaludo() {
        return "Hola desde mi carro";
    }
}
