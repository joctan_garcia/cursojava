package clasesnormales;

import clasesabstractas.ClaseAbstracta;

public class UsoDeClaseAbstracta extends ClaseAbstracta {

    @Override
    public void miMetodo() {
        System.out.println("Impresión desde un metodo abastacto");
    }
}
