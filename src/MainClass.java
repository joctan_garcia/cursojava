import java.util.Scanner;

public class MainClass {
    public static void main(String[] args) {
        Scanner miEscaner = new Scanner(System.in);
        String option = getOptionMenu();

        while (option != "c") {
            System.out.println("---------Bienvenido a mi culculadora---------");
            System.out.println("--Inserta un número--");

            int n1 = miEscaner.nextInt();

            System.out.println("--Inserta otro número--");

            int n2 = miEscaner.nextInt();


            int resultado;

            switch (option) {
                case "1": {
                    resultado = n1 + n2;
                    System.out.println(resultado);
                    break;
                }
                case "2": {
                    resultado = n1 - n2;
                    System.out.println(resultado);
                    break;
                }
                case "3": {
                    resultado = n1 * n2;
                    System.out.println(resultado);
                    break;
                }
                default:
                    System.out.println("Selecciona una opción aceptable");
            }

            option = getOptionMenu();
            System.out.println("Valor option: " + option);
        }
    }


    private static String getOptionMenu() {
        Scanner miEscaner = new Scanner(System.in);

        System.out.println("--¿Qué operación quieres realizar?--");
        System.out.println("-- 1 -> Suma --");
        System.out.println("-- 2 -> Resta --");
        System.out.println("-- 3 -> Multiplicación --");
        System.out.println("-- c -> Cancelar y salir --");

        return miEscaner.nextLine();
    }
}
